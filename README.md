**Dependencies:**
*  Python 3

**Python modules to install:**
* pyperclip
* mss
* keyboard 
* PIL 

**How to use it ?**
1. Run the script in a (Python) shell 
2. Make a screen shot by using the shortcut (ctrl+space by default) and choose a rectangle with the mouse in the frozen screen.
3. The paperclip contains the LaTex code to include the image.
4. Go back to to step 3 if necessary

**Notes:**
* you can change the parameters (file paths, defaults names,shortcut...) in the script
* the best way to use it is to create a copy of the script in the image directory of the latex document